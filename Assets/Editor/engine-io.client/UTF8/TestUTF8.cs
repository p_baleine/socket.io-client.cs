﻿using NUnit.Framework;
using System.Collections;

[TestFixture()]
public class TestUTF8
{
    [Test()]
    public void TestDecode ()
    {
        // 1-byte
        Assert.That (UTF8.Decode ("\0"), Is.EqualTo ("\0"));
        Assert.That (UTF8.Decode ("\x5C"), Is.EqualTo ("\x5C"));
        Assert.That (UTF8.Decode ("\x7F"), Is.EqualTo ("\x7F"));
        
        // 2-byte
        Assert.That (UTF8.Decode ("\xC2\x80"), Is.EqualTo ("\x80"));
        Assert.That (UTF8.Decode ("\xD7\x8A"), Is.EqualTo ("\u05CA"));
        Assert.That (UTF8.Decode ("\xDF\xBF"), Is.EqualTo ("\u07FF"));
        
        // 3-byte
        Assert.That (UTF8.Decode ("\xE0\xA0\x80"), Is.EqualTo ("\u0800"));
        Assert.That (UTF8.Decode ("\xE2\xB0\xBC"), Is.EqualTo ("\u2C3C"));
        Assert.That (UTF8.Decode ("\xEF\xBF\xBF"), Is.EqualTo ("\uFFFF"));
        
        // 4-byte
        Assert.That (UTF8.Decode ("\xF0\x90\x80\x80"), Is.EqualTo ("\uD800\uDC00"));
        Assert.That (UTF8.Decode ("\xF0\x9D\x8C\x86"), Is.EqualTo ("\uD834\uDF06"));
        Assert.That (UTF8.Decode ("\xF4\x8F\xBF\xBF"), Is.EqualTo ("\uDBFF\uDFFF"));
    }
    
    [Test()]
    public void TestEncode ()
    {
        // 1-byte
        Assert.That (UTF8.Encode ("\0"), Is.EqualTo ("\0"));
        Assert.That (UTF8.Encode ("\x5C"), Is.EqualTo ("\x5C"));
        Assert.That (UTF8.Encode ("\x7F"), Is.EqualTo ("\x7F"));
        
        // 2-byte
        Assert.That (UTF8.Encode ("\x80"), Is.EqualTo ("\xC2\x80"));
        Assert.That (UTF8.Encode ("\u05CA"), Is.EqualTo ("\xD7\x8A"));
        Assert.That (UTF8.Encode ("\u07FF"), Is.EqualTo ("\xDF\xBF"));
        
        // 3-byte
        Assert.That (UTF8.Encode ("\u0800"), Is.EqualTo ("\xE0\xA0\x80"));
        Assert.That (UTF8.Encode ("\u2C3C"), Is.EqualTo ("\xE2\xB0\xBC"));
        Assert.That (UTF8.Encode ("\uFFFF"), Is.EqualTo ("\xEF\xBF\xBF"));
        
        // 4-byte
        Assert.That (UTF8.Encode ("\uD800\uDC00"), Is.EqualTo ("\xF0\x90\x80\x80"));
        Assert.That (UTF8.Encode ("\uD834\uDF06"), Is.EqualTo ("\xF0\x9D\x8C\x86"));
        Assert.That (UTF8.Encode ("\uDBFF\uDFFF"), Is.EqualTo ("\xF4\x8F\xBF\xBF"));
    }
}
