﻿using NUnit.Framework;
using System.Collections;

[TestFixture]
public class EmitterTest
{
    Emitter e;

    [SetUp]
    public void Init ()
    {
        e = new Emitter ();
    }

    [Test]
    public void OnTest ()
    {
        bool called = false;
        e.On ("foo", x => called = true);
        e.Emit ("foo");
        Assert.That (called, Is.True);
        
        int[] args = null;
        e.On ("bar", x => {
            args = System.Array.ConvertAll(x, y => int.Parse (string.Format ("{0}", y)));
        });
        e.Emit ("bar", 1, 2, 3);
        Assert.That (args, Is.EqualTo (new int[] {1, 2, 3}));
    }
        
    [Test]
    public void OnceTest ()
    {
        int count = 0;
        e.Once ("foo", x => count++);
        e.Emit ("foo");
        e.Emit ("foo");
        Assert.AreEqual (1, count);
    }

    [Test]
    public void OffAllTest ()
    {
        bool calledFoo = false;
        bool calledBar = false;
        e.On ("foo", x => calledFoo = true);
        e.On ("bar", x => calledBar = true);
        e.Off ();
        e.Emit ("foo");
        e.Emit ("bar");
        Assert.That (calledFoo, Is.False);
        Assert.That (calledBar, Is.False);
    }

    [Test]
    public void OffEventTest ()
    {
        bool called = false;
        e.On ("foo", x => called = true);
        e.Off ("foo");
        e.Emit ("foo");
        Assert.That (called, Is.False);
    }

    [Test]
    public void OffCallbackTest ()
    {
        bool calledFoo = false;
        bool calledFoo2 = false;
        Emitter.EmitterDelegate d = x => calledFoo2 = true;
        e.On ("foo", x => calledFoo = true);
        e.On ("foo", d);
        e.Off ("foo", d);
        e.Emit ("foo");
        Assert.That (calledFoo, Is.True);
        Assert.That (calledFoo2, Is.False);
    }

    [Test]
    public void ListenersTest ()
    {
        e.On ("foo", x => {});
        e.On ("foo", x => {});
        Assert.That (e.Listeners ("foo").Count, Is.EqualTo (2));
    }

    [Test]
    public void HasListenersTest ()
    {
        Assert.That (e.HasListeners ("foo"), Is.False);
        e.On ("foo", x => {});
        Assert.That (e.HasListeners ("foo"), Is.True);
    }

    [Test]
    public void NotRegisteredEventTest ()
    {
        Assert.DoesNotThrow (() => e.Emit ("foo"));
    }
}