﻿using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace EngineIO
{
    [TestFixture()]
    public class TestEventThread
    {
        [Test()]
        public void TestIsCurren ()
        {
            BlockingQueue<bool> queue = new BlockingQueue<bool> ();
            
            queue.Enqueue (EventThread.IsCurrent);
            
            EventThread.Exec (() => {
                queue.Enqueue (EventThread.IsCurrent);
            });
            
            Assert.That (queue.Dequeue (), Is.False);
            Assert.That (queue.Dequeue (), Is.True);
        }
        
        [Test()]
        public void TestExec ()
        {
            BlockingQueue<int> queue = new BlockingQueue<int> ();
            
            EventThread.Exec (() => {
                queue.Enqueue (0);
                EventThread.Exec (() => {
                    queue.Enqueue (1);
                });
                queue.Enqueue (2);
            });
            
            EventThread.Exec (() => {
                queue.Enqueue (3);
            });
            
            for (int i = 0; i < 4; i++) {
                Assert.That (queue.Dequeue (), Is.EqualTo (i));
            }
        }
        
        [Test()]
        public void TestNextTick ()
        {
            BlockingQueue<int> queue = new BlockingQueue<int> ();
            HashSet<Thread> threads = new HashSet<Thread> ();
            
            EventThread.Exec (() => {
                threads.Add (Thread.CurrentThread);
                
                queue.Enqueue (0);
                EventThread.NextTick (() => {
                    threads.Add (Thread.CurrentThread);
                    queue.Enqueue (2);
                });
                queue.Enqueue (1);
            });
            
            for (int i = 0; i < 3; i++) {
                Assert.That (queue.Dequeue (), Is.EqualTo (i));
            }
            
            Assert.That (threads.Count, Is.EqualTo (1));
        }
        
        [Test()]
        public void TestExecWithException ()
        {
            EventThread.Exec (() => {
                throw new Exception ("test exception");
            });
        }
    }
}