﻿using NUnit.Framework;
using System.Collections;
using System.Text.RegularExpressions;

using EngineIO;

[TestFixture()]
public class TestParser
{
    public const string ERROR_DATA = "parser error";
    
    [Test()]
    public void TestEncodeAsString ()
    {
        Parser.EncodePacket (new Packet<string> (Packet.MESSAGE, "test"), (data) => {
            Assert.That (data, Is.InstanceOf (typeof (string)));
        });
    }
    
    [Test()]
    public void TestDecodeAsPacket ()
    {
        Parser.EncodePacket (new Packet<string> (Packet.MESSAGE, "test"), (data) => {
            Assert.That (Parser.DecodePacket ((string)data), Is.InstanceOf (typeof (Packet)));
        });
    }
    
    [Test()]
    public void TestNoData ()
    {
        Parser.EncodePacket (new Packet<string> (Packet.MESSAGE), (data) => {
            Packet<string> p = Parser.DecodePacket ((string)data);
            Assert.That (p.type, Is.EqualTo (Packet.MESSAGE));
            Assert.That (p.data, Is.Null);
        });
    }
    
    [Test()]
    public void TestEncodeOpenPacket ()
    {
        Parser.EncodePacket (new Packet<string> (Packet.OPEN, "{\"some\":\"json\"}"), (data) => {
            Packet<string> p = Parser.DecodePacket ((string)data);
            Assert.That (p.type, Is.EqualTo (Packet.OPEN));
            Assert.That (p.data, Is.EqualTo ("{\"some\":\"json\"}"));
        });
    }
    
    [Test()]
    public void TestEncodeClosePacket ()
    {
        Parser.EncodePacket (new Packet<string> (Packet.CLOSE), (data) => {
            Packet<string> p = Parser.DecodePacket ((string)data);
            Assert.That (p.type, Is.EqualTo (Packet.CLOSE));
        });
    }
    
    [Test()]
    public void TestEncodePingPacket ()
    {
        Parser.EncodePacket (new Packet<string> (Packet.PING, "1"), (data) => {
            Packet<string> p = Parser.DecodePacket ((string)data);
            Assert.That (p.type, Is.EqualTo (Packet.PING));
            Assert.That (p.data, Is.EqualTo ("1"));
        });
    }
    
    [Test()]
    public void TestEncodePongPacket ()
    {
        Parser.EncodePacket (new Packet<string> (Packet.PONG, "1"), (data) => {
            Packet<string> p = Parser.DecodePacket ((string)data);
            Assert.That (p.type, Is.EqualTo (Packet.PONG));
            Assert.That (p.data, Is.EqualTo ("1"));
        });
    }
    
    [Test()]
    public void TestEncodeMessagePacket ()
    {
        Parser.EncodePacket (new Packet<string> (Packet.MESSAGE, "aaa"), (data) => {
            Packet<string> p = Parser.DecodePacket ((string)data);
            Assert.That (p.type, Is.EqualTo (Packet.MESSAGE));
            Assert.That (p.data, Is.EqualTo ("aaa"));
        });
    }
    
    [Test()]
    public void TestEncodeUTF8SpecialCharsMessagePacket()
    {
        Parser.EncodePacket (new Packet<string> (Packet.MESSAGE, "utf8 — string"), (data) => {
            Packet<string> p = Parser.DecodePacket ((string)data);
            Assert.That (p.type, Is.EqualTo (Packet.MESSAGE));
            Assert.That (p.data, Is.EqualTo ("utf8 — string"));
        });
    }
    
    // Packet could not be so generic...
    // [Test()]
    // public void TestEncodeMessagePacketCoercingToString()
    // {
    //  Parser.EncodePacket (new Packet<int> (Packet.MESSAGE, 1), (data) => {
    //      Packet<string> p = Parser.DecodePacket (data);
    //      Assert.That (p.type, Is.EqualTo (Packet.MESSAGE));
    //      Assert.That (p.data, Is.EqualTo ("1"));
    //  });
    // }
    
    [Test()]
    public void TestEncodeUpgradePacket ()
    {
        Parser.EncodePacket (new Packet<string> (Packet.UPGRADE), (data) => {
            Packet<string> p = Parser.DecodePacket ((string)data);
            Assert.That (p.type, Is.EqualTo (Packet.UPGRADE));
        });
    }
    
    [Test()]
    public void TestEncodingFormat ()
    {
        Parser.EncodePacket (new Packet<string> (Packet.MESSAGE, "test"), (data) => {
            Assert.That (Regex.IsMatch ((string)data, "[0-9].*"), Is.True);
        });
        Parser.EncodePacket (new Packet<string> (Packet.MESSAGE), (data) => {
            Assert.That (Regex.IsMatch ((string)data, "[0-9]"), Is.True);
        });
    }
    
    [Test()]
    public void TestDecodeBadFormat ()
    {
        Packet<string> p = Parser.DecodePacket (":::");
        Assert.That (p.type, Is.EqualTo (Packet.ERROR));
        Assert.That (p.data, Is.EqualTo (ERROR_DATA));
    }
    
    [Test()]
    public void TestDecodeInexistentTypes ()
    {
        Packet<string> p = Parser.DecodePacket ("94103");
        Assert.That (p.type, Is.EqualTo (Packet.ERROR));
        Assert.That (p.data, Is.EqualTo (ERROR_DATA));
    }
    
    [Test()]
    public void TestDecodeInvalidUTF8 ()
    {
        Packet<string> p = Parser.DecodePacket ("4\uffff", true);
        Assert.That (p.type, Is.EqualTo (Packet.ERROR));
        Assert.That (p.data, Is.EqualTo (ERROR_DATA));
    }
    
    [Test()]
    public void TestEncodeBinaryMessage ()
    {
        byte[] data = new byte[5];
        for (int i = 0; i < data.Length; i++) {
            data[0] = (byte)i;
        }
        Parser.EncodePacket (new Packet<byte[]> (Packet.MESSAGE, data), (encoded) => {
            Packet<byte[]> p = Parser.DecodePacket ((byte[])encoded);
            Assert.That (p.type, Is.EqualTo (Packet.MESSAGE));
            Assert.That (p.data, Is.EqualTo (data));
        });
    }
}

