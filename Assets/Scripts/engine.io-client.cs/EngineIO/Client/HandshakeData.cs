using System;
using System.Collections;
using System.Collections.Generic;

using EngineIO.Client;
using EngineIO.Client.Transports;
using ParseQS;

namespace EngineIO
{
	namespace Client
	{
		class HandshakeData
		{
            public string sid;
            public string[] upgrades;
            public long pingInterval;
            public long pingTimeout;

			public HandshakeData (string data) : this (JSON.Parse (data) as Dictionary<string, object>)
			{
			}

            public HandshakeData (Dictionary<string, object> data)
            {
				List<object> upgrades = (List<object>)data ["upgrades"];
				string[] _upgrades = new string[upgrades.Count];
                for (int i = 0; i < upgrades.Count; i++) {
                    _upgrades[i] = (string)upgrades[i];
				}

                this.sid = (string)data ["sid"];
                this.upgrades = _upgrades;
                this.pingInterval = (long)data ["pingInterval"];
                this.pingTimeout = (long)data ["pingTimeout"];
			}
		}

	}
}
