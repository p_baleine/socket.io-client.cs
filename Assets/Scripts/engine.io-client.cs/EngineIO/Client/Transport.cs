﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace EngineIO
{
    namespace Client
    {
        public abstract class Transport : Emitter
        {
            protected enum ReadyState
            {
				CLOSED, OPENING, OPEN, PAUSED
            }
            
            public const string EVENT_OPEN = "open";
            public const string EVENT_CLOSE = "close";
            public const string EVENT_PACKET = "packet";
            public const string EVENT_DRAIN = "drain";
            public const string EVENT_ERROR = "error";
            public const string EVENT_REQUEST_HEADERS = "requestHeaders";
            public const string EVENT_RESPONSE_HEADERS = "responseHeaders";
            
            protected static int timestamps = 0;
            
            public bool writable;
            public string name;
            public Dictionary<string, string> query;
            
            protected bool secure;
            protected bool timestampRequests;
            protected int port;
            protected string path;
            protected string hostname;
            protected string timestampParam;
            // protected SSLContext sslContext;
            protected Socket socket;
            
            protected ReadyState readyState;
            
            public Transport (Options opts)
            {
                this.path = opts.path;
                this.hostname = opts.hostname;
                this.port = opts.port;
                this.secure = opts.secure;
                this.query = opts.query;
                this.timestampParam = opts.timestampParam;
                this.timestampRequests = opts.timestampRequests;
                //this.sslContext = opts.sslContext;
                this.socket = opts.socket;
            }
            
            protected Transport OnError (string msg, Exception desc)
            {
                Exception err = new EngineIOException (msg, desc);
                this.Emit (EVENT_ERROR, err);
                return this;
            }
            
            public Transport Open ()
            {
                EventThread.Exec (() => {
                    if (this.readyState == ReadyState.CLOSED || this.readyState == null) {
                        readyState = ReadyState.OPENING;
                        DoOpen ();
                    }
                });
                return this;
            }
            
            public Transport Close ()
            {
                EventThread.Exec (() => {
                    if (readyState == ReadyState.OPENING || readyState == ReadyState.OPEN) {
                        DoClose ();
                        OnClose ();
                    }
                });
                return this;
            }
            
            public void Send (Packet[] packets)
            {
                EventThread.Exec (() => {
                    if (readyState == ReadyState.OPEN) {
                        Write (packets);
                    } else {
                        throw new SystemException ("Transport not open");
                    }
                });
            }
            
            protected void OnOpen ()
            {
                readyState = ReadyState.OPEN;
                writable = true;
                Emit (EVENT_OPEN);
            }
            
            protected void OnData (string data)
            {
				Packet<string> p = Parser.DecodePacket (data);
				OnPacket (Parser.DecodePacket (data));
            }
            
            protected void OnData (byte[] data)
            {
                OnPacket (Parser.DecodePacket (data));
            }
            
            protected void OnPacket (Packet packet)
            {
                Emit (EVENT_PACKET, packet);
			}
            
            protected void OnClose ()
            {

                readyState = ReadyState.CLOSED;
                Emit (EVENT_CLOSE);
            }
            
            abstract protected void Write (Packet[] packets);
            
            abstract protected void DoOpen ();
            
            abstract protected void DoClose ();
            
            public class Options
            {
                public string hostname;
                public string path;
                public string timestampParam;
                public bool secure;
                public bool timestampRequests;
                public int port = -1;
                public int policyPort = -1;
                public Dictionary<string, string> query;
                public Socket socket;
            }
        }

    }
}
