using System.Collections;
using UnityEngine;
using UnityDebug = UnityEngine.Debug;

namespace EngineIO
{
	namespace Client
	{
        // TODO Are there any necessity using UnityEngine.Debug?

		public class Logger
		{
            public void Debug (string message)
            {
                UnityDebug.Log (message);
            }

            public void Fine (string message)
            {
                UnityDebug.Log (message);
            }
		}
	}
}
