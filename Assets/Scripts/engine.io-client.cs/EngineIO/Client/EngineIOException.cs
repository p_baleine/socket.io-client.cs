using System;
using System.Collections;
using System.Collections.Generic;

namespace EngineIO
{
    namespace Client
    {
        class EngineIOException : Exception
        {
            public EngineIOException ()
            {
            }
            
            public EngineIOException (string message) : base(message)
            {
            }
            
            public EngineIOException (string message, Exception inner) : base(message, inner)
            {
            }
        }
    }
}
