﻿using System;
using System.Collections;
using System.Collections.Generic;

using WebSocketClient = WebSocketSharp.WebSocket;

using ParseQS;

namespace EngineIO
{
    namespace Client
    {
        namespace Transports
        {
            public class WebSocket : Transport
            {
                public const string NAME = "websocket";
                
                private WebSocketClient ws;
                
                public WebSocket (Options opts) : base (opts)
                {
                    this.name = NAME;
                }
                
                protected override void DoOpen ()
                {
                    if (!check ()) {
                        return;
                    }
                    
                    try {
                        ws = new WebSocketClient (uri);
                    } catch (ArgumentException e) {
                        ws.Close ();
                        throw new SystemException ("websocket error", e);
                    }
                    
                    // TODO using could be used?
                    
                    ws.OnOpen += (sender, e) => {
                        EventThread.Exec (() => {
                            // TODO header?
                            this.OnOpen ();
                        });
                    };

                    ws.OnClose += (sender, e) => {
                        EventThread.Exec (() => {
                            this.OnClose ();
                        });
                    };

                    ws.OnMessage += (sender, e) => {
                        EventThread.Exec (() => {
                            if (e.Type == WebSocketSharp.Opcode.BINARY) {
                                this.OnData (e.RawData);
                            } else {
                                this.OnData (e.Data);
                            }
                        });
                    };

                    ws.OnError += (sender, e) => {
                        EventThread.Exec (() => {
                            this.OnError ("websocket error", new Exception (e.Message));
                        });
                    };
                    
                    ws.Connect ();
                }
                
                protected override void Write (Packet[] packets)
                {
                    this.writable = false;
                    foreach (var packet in packets) {
                        Parser.EncodePacket (packet, (p) => {
                            if (p is String) {
                                this.ws.Send ((string)p);
                            } else if (p is Byte[]) {
                                this.ws.Send ((byte[])p);
                            }
                        });
                    }

                    EventThread.Exec (() => {
                        writable = true;
                        Emit (EVENT_DRAIN);
                    });
                }
                
                protected override void DoClose ()
                {
                    if (ws != null) {
                        ws.Close ();
                    }
                }
                
                protected string uri {
                    get {
                        Dictionary<string, string> query = this.query;
                        if (query == null) {
                            query = new Dictionary<string, string> ();
                        }
                        string schema = this.secure ? "wss" : "ws";
                        string port = "";
                        
                        if (this.port > 0 && (("wss".Equals (schema) && this.port != 443)
                                              || ("ws".Equals (schema) && this.port != 80))) {
                            port = ":" + this.port;
                        }
                        
                        if (this.timestampRequests) {
                            query.Add (this.timestampParam,
                                       (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds.ToString ());
                        }
                        
                        string _query = Querystring.Encode (query);
                        if (_query.Length > 0) {
                            _query = "?" + _query;
                        }
                        
                        return schema + "://" + this.hostname + port + this.path + _query;
                    }
                }
                
                private bool check ()
                {
                    return true;
                }
            }
        }
    }
}
