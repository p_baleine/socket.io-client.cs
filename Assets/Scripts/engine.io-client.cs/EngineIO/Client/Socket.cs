using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

using EngineIO.Client;
using EngineIO.Client.Transports;
using ParseQS;

// TODO thread safe
// TODO asset store directory structure and dependency management
// TODO push to github
// TODO sslContext
// TODO upgrade
// TODO api doc

namespace EngineIO
{
    namespace Client
    {
        public class Socket : Emitter
        {
            public static Logger logger = new Logger ();

            private enum ReadyState
            {
                OPENING, OPEN, CLOSING, CLOSED
            }

            public delegate void SendDelegate ();

            private delegate void CloseDelegate ();
            private delegate void WaitForUpgradeDelegate ();

            public const string EVENT_OPEN = "open";
            public const string EVENT_CLOSE = "close";
            public const string EVENT_MESSAGE = "message";
            public const string EVENT_ERROR = "error";
            public const string EVENT_UPGRADE_ERROR = "upgradeError";
            public const string EVENT_FLUSH = "flush";
            public const string EVENT_DRAIN = "drain";
            public const string EVENT_HANDSHAKE = "handshake";
            public const string EVENT_UPGRADING = "upgrading";
            public const string EVENT_UPGRADE = "upgrade";
            public const string EVENT_PACKET = "packet";
            public const string EVENT_PACKET_CREATE = "packetCreate";
            public const string EVENT_HEARTBEAT = "heartbeat";
            public const string EVENT_DATA = "data";
            public const string EVENT_TRANSPORT = "transport";

            public static int protocol = Parser.protocol;

            public static bool priorWebsocketSuccess = false;

            // public static SSLContext defaultSSLContext;

            private bool secure;
            private bool upgrade;
            private bool timestampRequests;
            private bool upgrading;
            private bool rememberUpgrade;
            private int port;
            private int policyPort;
            private int prevBufferLen;
            private long pingInterval;
            private long pingTimeout;
            private string _id;
            private string hostname;
            private string path;
            private string timestampParam;
            private List<string> transports;
            private List<string> upgrades;
            private Dictionary<string, string> query;
            private Queue<Packet> weiteBuffer = new Queue<Packet> (); // TODO is this thread safe?
            private Queue<SendDelegate> callbackBuffer = new Queue<SendDelegate> ();
            private Transport transport;
            private Timer pingTimueoutTimer;
            private Timer pingIntervalTimer;
            // private SSLContext sslContext;

            private ReadyState readyState;

            public Socket () : this (new Options ())
            {
            }

            public Socket (string uri) : this (uri, null)
            {
            }

            public Socket (Uri uri) : this (uri, null)
            {
            }

            public Socket (string uri, Options opts) : this (uri == null ? null : new Uri (uri), opts)
            {
            }

            public Socket (Uri uri, Options opts) : this (uri == null ? opts : Options.FromURI (uri, opts))
            {
            }

            public Socket (Options opts)
            {
				this.secure = opts.secure;

                if (opts.host != null) {
                    bool ipv6uri = opts.host.IndexOf (']') != -1;
                    string[] pieces = ipv6uri ? opts.host.Split (new string[] {"]:"}, StringSplitOptions.None) :
                        opts.host.Split (':');
                    bool ipv6 = (pieces.Length > 2 || opts.host.IndexOf ("::") == -1);
                    if (ipv6) {
                        opts.hostname = opts.host;
                    } else {
                        opts.hostname = pieces[0];
                        if (ipv6uri) {
                            opts.hostname = opts.hostname.Substring (1);
                        }
                        if (pieces.Length > 1) {
                            opts.port = int.Parse (pieces[pieces.Length - 1]);
                        } else if (opts.port == -1) {
                            // If no port is specified manually, use the protocol default.
                            opts.port = this.secure ? 443 : 80;
                        }
                    }
                }

                this.hostname = opts.hostname != null ? opts.hostname : "localhost";
                this.port = opts.port != 0 ? opts.port : (this.secure ? 443 : 80);
                this.query = opts.query != null ?
                    Querystring.Decode (opts.query) : new Dictionary<string, string> ();
                this.upgrade = opts.upgrade;
                this.path = (opts.path != null ? opts.path : "/engine.io").Replace ("/$", "") + "/";
                this.timestampParam = opts.timestampParam != null ? opts.timestampParam : "t";
                this.timestampRequests = opts.timestampRequests;
                this.transports = new List<string> (opts.transports != null ?
                    opts.transports : new string[] { WebSocket.NAME });
                this.policyPort = opts.policyPort != 0 ? opts.policyPort : 843;
                this.rememberUpgrade = opts.rememberUpgrade;
            }

            public Socket Open ()
            {
                EventThread.Exec (() => {
                    string transportName;
                    if (rememberUpgrade && /* priorWebSocketSuccess && */ transports.Contains (WebSocket.NAME)) {
                        transportName = WebSocket.NAME;
                    } else if (0 == transports.Count) {
                        // Emit error on next tick (?) so it can be listened to
                        EventThread.NextTick (() => {
                            Emit (EVENT_ERROR, new EngineIOException ("No transports available"));
                        });
                        return;
                    } else {
                        transportName = transports[0];
                    }
                    readyState = ReadyState.OPENING;
                    Transport transport = createTransport (transportName);
                    setTransport (transport);
                    transport.Open ();
                });

                return this;
            }

            private Transport createTransport (string name)
            {
                logger.Fine (String.Format ("creating transport '{0}'", name));
                Dictionary<string, string> query = new Dictionary<string, string> (this.query);

                query ["EIO"] = Parser.protocol.ToString ();
                query ["transport"] = name;
                if (id != null) {
                    query ["sid"] = id;
                }

                Transport.Options opts = new Transport.Options ();
                opts.hostname = hostname;
                opts.port = port;
                opts.secure = secure;
                opts.path = path;
                opts.query = query;
                opts.timestampRequests = timestampRequests;
                opts.timestampParam = timestampParam;
                opts.policyPort = policyPort;
                opts.socket = this;

                if (WebSocket.NAME.Equals (name)) {
                    return new WebSocket (opts);
                } // else ...

                throw new SystemException ();
            }

            private void setTransport (Transport transport)
            {
                logger.Fine (String.Format ("setting transport {0}", transport.name));

                if (this.transport != null) {
                    logger.Fine (String.Format ("clearing existing transport {0}", this.transport.name));
                    this.transport.Off ();
                }

                this.transport = transport;

                Emit (Transport.EVENT_DRAIN, transport);

                transport.On (Transport.EVENT_DRAIN, (args) => {
                    onDrain ();
                }).On (Transport.EVENT_PACKET, (args) => {
                    Packet packet = args.Length > 0 ? (Packet)args [0] : null;
                    if (packet is Packet<string>) {
                        onPacket<string> ((Packet<string>)packet);
                    } else {
                        onPacket<byte[]> ((Packet<byte[]>)packet);
                    }
                }).On (Transport.EVENT_ERROR, (args) => {
                    onError (args.Length > 0 ? (Exception)args [0] : null);
                }).On (Transport.EVENT_CLOSE, (args) => {
                    onClose ("transport close");
                });
            }

            private void probe (string name)
            {
                throw new NotImplementedException ();
            }

            private void onOpen ()
            {
                logger.Fine ("socket open");
                readyState = ReadyState.OPEN;
                // Socket.priorWebsocketSuccess = WebSocket.NAME.Equals (tranport.name);
                Emit (EVENT_OPEN);
                flush ();

                // if (readyState == ReadyState.OPEN && upgrade && transport is Polling) {
                // }
            }

            private void onPacket<T> (Packet<T> packet) where T : class
            {
                if (readyState == ReadyState.OPENING || readyState == ReadyState.OPEN) {
                    logger.Fine (String.Format ("socket received: type {0}, data {1}", packet.type, packet.data));

                    Emit (EVENT_PACKET, packet);
                    Emit (EVENT_HEARTBEAT);

                    if (Packet.OPEN.Equals (packet.type)) {
                        onHandshake (new HandshakeData ((string)(object)packet.data));
                    } else if (Packet.PONG.Equals (packet.type)) {
                        setPing ();
                    } else if (Packet.ERROR.Equals (packet.type)) {
                        EngineIOException err = new EngineIOException ("server error");
                        Emit (EVENT_ERROR, err);
                    } else if (Packet.MESSAGE.Equals (packet.type)) {
                        Emit (EVENT_DATA, packet.data);
                        Emit (EVENT_MESSAGE, packet.data);
                    }

                } else {
                    logger.Fine (String.Format ("packet received with socket readyState '{0}'", readyState));
                }
            }

            private void onHandshake (HandshakeData data)
            {
				Emit (EVENT_HANDSHAKE, data);
                id = data.sid;
                transport.query ["sid"] = data.sid;
                upgrades = filterUpgrades (new List<string> (data.upgrades));
                pingInterval = data.pingInterval;
                pingTimeout = data.pingTimeout;
                onOpen ();
                // In case open handler closes socket
                if (ReadyState.CLOSED == readyState)
                    return;
                setPing ();

                Off (EVENT_HEARTBEAT, onHeartbeatAsListener);
                On (EVENT_HEARTBEAT, onHeartbeatAsListener);
			}

            private void onHeartbeatAsListener (params object[] args)
            {
                onHeartbeat (args.Length > 0 ? (long)args [0] : 0);
            }

            private void onHeartbeat (long timeout)
            {
                if (pingTimueoutTimer != null) {
                    pingTimueoutTimer.Dispose ();
 //                   pingTimueoutTimer.Change (Timeout.Infinite, Timeout.Infinite);
                }

                if (timeout <= 0) {
                    timeout = pingInterval + pingTimeout;
                }

                pingTimueoutTimer = new Timer ((state) => {
                    EventThread.Exec (() => {
                        if (readyState == ReadyState.CLOSED)
                            return;
                        onClose ("ping timeout");
                    });
                }, null, 0, timeout);
            }

            private void setPing ()
            {
                if (pingIntervalTimer != null) {
                    pingIntervalTimer.Dispose ();
                    //pingIntervalTimer.Change (Timeout.Infinite, Timeout.Infinite);
                }

                pingIntervalTimer = new Timer ((state) => {
                    EventThread.Exec (() => {
                        logger.Fine (string.Format ("writing ping packet - expecting pong within {0}ms", pingTimeout));
                        Ping ();
                        onHeartbeat (pingTimeout);
                    });
                }, null, 0, pingInterval);
            }

            public void Ping ()
            {
                EventThread.Exec (() => {
                    sendPacket (Packet.PING);
                });
            }

            private void onDrain ()
            {
                SendDelegate[] _callbackBuffer = callbackBuffer.ToArray ();
                for (int i = 0; i < prevBufferLen; i++) {
                    SendDelegate callback = _callbackBuffer[i];
                    if (callback != null) {
                        callback();
                    }
                }

                for (int i = 0; i < prevBufferLen; i++) {
                    weiteBuffer.Dequeue ();
                    callbackBuffer.Dequeue ();
                }

                prevBufferLen = 0;
                if (weiteBuffer.Count == 0) {
                    Emit (EVENT_DRAIN);
                } else {
                    flush ();
                }
            }

            private void flush ()
            {
                if (readyState != ReadyState.CLOSED && transport.writable &&
                    !upgrading && weiteBuffer.Count != 0) {
                    logger.Fine (String.Format ("flushing {0} packets in socket", weiteBuffer.Count));
                    prevBufferLen = weiteBuffer.Count;
                    transport.Send (weiteBuffer.ToArray ());
                    Emit (EVENT_FLUSH);
                }
            }

            public void Write (string msg, SendDelegate fn)
            {
                Send (msg, fn);
            }

            public void Write (byte[] msg, SendDelegate fn)
            {
                Send (msg, fn);
            }

            public void Send (string msg)
            {
                Send (msg, null);
            }

            public void Send (byte[] msg)
            {
                Send (msg, null);
            }

            public void Send (string msg, SendDelegate fn)
            {
                EventThread.Exec (() => {
                    sendPacket (Packet.MESSAGE, msg, fn);
                });
            }

            public void Send (byte[] msg, SendDelegate fn)
            {
                EventThread.Exec (() => {
                    sendPacket (Packet.MESSAGE, msg, fn);
                });
            }

            private void sendPacket (string type)
            {
                sendPacket (new Packet<string> (type), null);
            }

            private void sendPacket (string type, string data, SendDelegate fn)
            {
                Packet packet = new Packet<string> (type, data);
                sendPacket (packet, fn);
            }

            private void sendPacket (string type, byte[] data, SendDelegate fn)
            {
                Packet packet = new Packet<byte[]> (type, data);
                sendPacket (packet, fn);
            }

            private void sendPacket (Packet packet, SendDelegate fn)
            {
                if (ReadyState.CLOSING == readyState || ReadyState.CLOSED == readyState) {
                    return;
                }

                if (fn == null) {
                    fn = () => {};
                }

                Emit (EVENT_PACKET_CREATE, packet);
                weiteBuffer.Enqueue (packet);
                callbackBuffer.Enqueue (fn);
                flush ();
            }

            public Socket Close ()
            {
                EventThread.Exec (() => {
                    if (readyState == ReadyState.OPENING || readyState == ReadyState.OPEN) {
                        readyState = ReadyState.CLOSING;

                        CloseDelegate close = () => {
                            onClose ("forced close");
                            logger.Fine ("socket closing - telling transport to close");
                            transport.Close ();
                        };

                        EmitterDelegate[] cleanupAndClose = new EmitterDelegate[1];
                        cleanupAndClose[0] = (args) => {
                            Off (EVENT_UPGRADE, cleanupAndClose[0]);
                            Off (EVENT_UPGRADE_ERROR, cleanupAndClose[0]);
                            close ();
                        };

                        WaitForUpgradeDelegate waitForUpgrade = () => {
                            // wait for update to finish since we can't send packets while pausing a transport
                            Once (EVENT_UPGRADE, cleanupAndClose[0]);
                            Once (EVENT_UPGRADE_ERROR, cleanupAndClose[0]);
                        };

                        if (weiteBuffer.Count > 0) {
                            Once (EVENT_DRAIN, (args) => {
                                if (upgrading) {
                                    waitForUpgrade ();
                                } else {
                                    close ();
                                }
                            });
                        } else if (upgrading) {
                            waitForUpgrade ();
                        } else {
                            close ();
                        }
                    }
                });
                return this;
            }

            private void onError (Exception err)
            {
                logger.Fine (String.Format ("socket error {0}", err.ToString ()));
                priorWebsocketSuccess = false;
                Emit (EVENT_ERROR, err);
                onClose ("transport error", err);
            }

            private void onClose (string reason)
            {
                onClose (reason, null);
            }

            private void onClose (string reason, Exception desc)
            {
                if (ReadyState.OPENING == readyState || ReadyState.OPEN == readyState || ReadyState.CLOSING == readyState) {
                    logger.Fine (String.Format ("socket close with reason: {0}", reason));

                    // clear timers
                    if (pingIntervalTimer != null) {
                        pingIntervalTimer.Dispose ();
                        // pingIntervalTimer.Change (Timeout.Infinite, Timeout.Infinite);
                    }
                    if (pingTimueoutTimer != null) {
                        pingTimueoutTimer.Dispose ();
                        //pingTimueoutTimer.Change (Timeout.Infinite, Timeout.Infinite);
                    }

                    EventThread.NextTick (() => {
                        weiteBuffer.Clear ();
                        callbackBuffer.Clear ();
                        prevBufferLen = 0;
                    });

                    // stop event from firing again for transport
                    transport.Off (EVENT_CLOSE);

                    // ensure transport won't stay open
                    transport.Close ();

                    // ignore futher transport communication
                    transport.Off ();

                    // set ready state
                    readyState = ReadyState.CLOSING;

                    // clear session id
                    id = null;

                    // emit close events
                    Emit (EVENT_CLOSE, reason, desc);
                }
            }

            private List<string> filterUpgrades(List<string> upgrades) {
                List<string> filterUpgrades = new List<string> ();
                foreach (var upgrade in upgrades) {
                    if (transports.Contains (upgrade)) {
                        filterUpgrades.Add (upgrade);
                    }
                }

                return filterUpgrades;
            }

            public string id {
                get {
                    return _id;
                }
                private set {
                    _id = value;
                }
            }

            public class Options : Transport.Options
            {
                public string[] transports;
                public bool upgrade = true;
                public bool rememberUpgrade;
                public string host;
                public string query;

                public static Options FromURI (Uri uri, Options opts)
                {
                    if (opts == null) {
                        opts = new Options ();
                    }

                    opts.host = uri.Host;
                    opts.secure = "https".Equals (uri.Scheme) || "wss".Equals (uri.Scheme);
                    opts.port = uri.Port;

                    string query = uri.Query == string.Empty ? null : uri.Query;
                    if (query != null) {
                        opts.query = query;
                    }

                    return opts;
                }
            }
        }
    }
}
