﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace EngineIO
{
    public class Parser
    {
        // TODO payload support
        
        private static int MAX_INT_CHAR_LENGTH = int.MaxValue.ToString().Length;
        
        public const int protocol = 3;
        
        private static Dictionary<string, int> packets = new Dictionary<string, int> {
            { Packet.OPEN, 0 },
            { Packet.CLOSE, 1 },
            { Packet.PING, 2 },
            { Packet.PONG, 3 },
            { Packet.MESSAGE, 4 },
            { Packet.UPGRADE, 5 },
            { Packet.NOOP, 6 }
        };
        
        private static Dictionary<int, string> packetslist = new Dictionary<int, string> ();
        
        private static Packet<string> err = new Packet<string> (Packet.ERROR, "parser error");
        
        static Parser ()
        {
            foreach (var packet in packets) {
                packetslist.Add (packet.Value, packet.Key);
            }
        }
        
        private Parser ()
        {
        }
        
        public static void EncodePacket (Packet packet, EncodeDelegate callback)
        {
            EncodePacket (packet, false, callback);
        }
        
        
        public static void EncodePacket (Packet packet, bool utf8encode, EncodeDelegate callback)
        {
            if (packet is Packet<byte[]>) {
                Packet<byte[]> _packet = (Packet<byte[]>)packet;
                EncodeDelegate<byte[]> _callback = new EncodeDelegate<byte[]> (callback);
                EncodeByteArray (_packet, _callback);
            } else {
                Packet<string> _packet = (Packet<string>) packet;
                EncodeDelegate<string> _callback = new EncodeDelegate<string> (callback);
                string encoded = Convert.ToString ((int)packets [_packet.type]);
                if (null != _packet.data) {
                    encoded += utf8encode ? UTF8.Encode (_packet.data) : _packet.data;
                }
                callback (encoded);
            }
        }
        
        private static void EncodeByteArray (Packet<byte[]> packet, EncodeDelegate<byte[]> callback)
        {
            byte[] data = packet.data;
            byte[] resultArray = new byte[1 + data.Length];
            resultArray [0] = Convert.ToByte (packets [packet.type]);
            Array.Copy (data, 0, resultArray, 1, data.Length);
            callback (resultArray);
        }
        
        public static Packet<string> DecodePacket (string data)
        {
            return DecodePacket (data, false);
        }
        
        public static Packet<string> DecodePacket (string data, bool utf8decode)
        {
            int type;
            
            try {
                type = (int) Char.GetNumericValue (data[0]);
            } catch (IndexOutOfRangeException e) {
                type = -1;
            }
            
            if (utf8decode) {
                try {
                    data = UTF8.Decode (data);
                } catch (UTF8Exception e) {
                    return err;
                }
            }
            
            if (type < 0 || type > packetslist.Count) {
                return err;
            }
            
            if (data.Length > 1) {
                return new Packet<string> (packetslist [type], data.Substring (1));
            } else {
                return new Packet<string> (packetslist [type]);
            }
        }
        
        public static Packet<byte[]> DecodePacket (byte[] data)
        {
            int type = data [0];
            byte[] intArray = new byte[data.Length - 1];
            Array.Copy (data, 1, intArray, 0, intArray.Length);
            return new Packet<byte[]> (packetslist [type], intArray);
        }
        
        public delegate void EncodeDelegate (object data);
        public delegate void EncodeDelegate<T> (T data);
    }

    public class Packet
    {
        public const string OPEN = "open";
        public const string CLOSE = "close";
        public const string PING = "ping";
        public const string PONG = "pong";
        public const string UPGRADE = "upgrade";
        public const string MESSAGE = "message";
        public const string NOOP = "noop";
        public const string ERROR = "error";
    }
    
    public class Packet<T> : Packet where T : class
    {       
        public string type;
        public T data;
        
        public Packet (string type) : this (type, null) {}
        
        public Packet (string type, T data)
        {
            this.type = type;
            this.data = data;
        }
    }
}
