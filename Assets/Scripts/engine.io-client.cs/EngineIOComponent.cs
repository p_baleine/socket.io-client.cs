﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

using EngineIO.Client;

public class EngineIOComponent : MonoBehaviour
{
    public Text text;

    private Socket socket;

    private Queue queue = Queue.Synchronized (new Queue ());

    // TODO disposal?

	void Start ()
    {
        socket = new Socket ("ws://192.168.1.29:4567");
        socket.On (Socket.EVENT_OPEN, (_args) => {
            Debug.Log ("socket open!!!");
            socket.Send ("hi");
            socket.On (Socket.EVENT_MESSAGE, (args) => {
                try {
                    Debug.Log ("on message!!!");
                    string msg = (string)args [0];
                    queue.Enqueue (msg);
                    if (msg == "bye") {
                        socket.Close ();
                    }
                    Debug.Log ((string)args [0]);
                    Debug.Log ("on  after message!!!");

                } catch (Exception e) {
                    Debug.Log (e);
                }
            });
        });
        socket.Open ();
	}

    void Update ()
    {
        lock (queue) {
            foreach (var msg in queue) {
                text.text = (string)msg;
            }
        }
    }
}
