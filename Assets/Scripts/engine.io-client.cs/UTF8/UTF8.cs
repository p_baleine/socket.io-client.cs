﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

public class UTF8
{
    private static int[] byteArray;
    private static int byteCount;
    private static int byteIndex;
    
    public static string Encode (string str)
    {
        int[] codePoints = Uc2decode (str);
        int length = codePoints.Length;
        int index = -1;
        int codePoint;
        StringBuilder byteString = new StringBuilder ();
        while (++index < length) {
            codePoint = codePoints[index];
            byteString.Append (EncodeCodePoint (codePoint));
        }
        return byteString.ToString ();
    }
    
    public static string Decode (string byteStr)
    {
        byteArray = Uc2decode (byteStr);
        byteCount = byteArray.Length;
        byteIndex = 0;
        List<int> codePoints = new List<int> ();
        int tmp;
        while ((tmp = decodeSymbol()) != -1) {
            codePoints.Add (tmp);
        }
        return Ucs2Encode (codePoints.ToArray ());
    }
    
    private static int[] Uc2decode (string str)
    {
        int length = str.Length;
        StringInfo strInfo = new StringInfo (str);
        int[] output = new int[strInfo.LengthInTextElements];
        int counter = 0;
        for (int i = 0; i < length; i += char.IsSurrogatePair (str, i) ? 2 : 1) {
            output[counter++] = char.ConvertToUtf32 (str, i);
        }
        return output;
    }
    
    private static string EncodeCodePoint (int codePoint)
    {
        StringBuilder symbol = new StringBuilder ();
        if ((codePoint & 0xFFFFFF80) == 0) {
            return symbol.Append (Convert.ToChar (codePoint)).ToString ();
        }
        if ((codePoint & 0xFFFFF800) == 0) {
            symbol.Append (Convert.ToChar (((codePoint >> 6) & 0x1F) | 0xC0));
        } else if ((codePoint & 0xFFFF0000) == 0) {
            symbol.Append (Convert.ToChar (((codePoint >> 12) & 0x0F) | 0xE0));
            symbol.Append (CreateByte(codePoint, 6));
        } else if ((codePoint & 0xFFE00000) == 0) {
            symbol.Append (Convert.ToChar (((codePoint >> 18) & 0x07) | 0xF0));
            symbol.Append (CreateByte(codePoint, 12));
            symbol.Append (CreateByte(codePoint, 6));
        }
        symbol.Append (Convert.ToChar ((codePoint & 0x3F) | 0x80));
        return symbol.ToString ();
    }
    
    private static char CreateByte (int codePoint, int shift)
    {
        return Convert.ToChar (((codePoint >> shift) & 0x3F) | 0x80);
    }
    
    public static int decodeSymbol ()
    {
        int byte1;
        int byte2;
        int byte3;
        int byte4;
        int codePoint;
        
        if (byteIndex > byteCount) {
            throw new UTF8Exception ("Invalid byte index");
        }
        
        if (byteIndex == byteCount) {
            return -1;
        }
        
        byte1 = byteArray [byteIndex] & 0xFF;
        byteIndex++;
        
        if ((byte1 & 0x80) == 0) {
            return byte1;
        }
        
        if ((byte1 & 0xE0) == 0xC0) {
            byte2 = ReadContinuationByte ();
            codePoint = ((byte1 & 0x1F) << 6) | byte2;
            if (codePoint >= 0x80) {
                return codePoint;
            } else {
                throw new UTF8Exception ("Invalid continuation byte");
            }
        }
        
        if ((byte1 & 0xF0) == 0xE0) {
            byte2 = ReadContinuationByte ();
            byte3 = ReadContinuationByte ();
            codePoint = ((byte1 & 0x0F) << 12) | (byte2 << 6) | byte3;
            if (codePoint >= 0x0800) {
                return codePoint;
            } else {
                throw new UTF8Exception ("Invalid continuation byte");
            }
        }
        
        if ((byte1 & 0xF8) == 0xF0) {
            byte2 = ReadContinuationByte ();
            byte3 = ReadContinuationByte ();
            byte4 = ReadContinuationByte ();
            codePoint = ((byte1 & 0x0F) << 0x12) | (byte2 << 0x0C) | (byte3 << 0x06) | byte4;
            if (codePoint >= 0x010000 && codePoint <= 0x10FFFF) {
                return codePoint;
            }
        }
        
        throw new UTF8Exception ("Invalid continuation byte");
    }
    
    static int ReadContinuationByte ()
    {
        if (byteIndex >= byteCount) {
            throw new UTF8Exception ("Invalit byte index");
        }
        
        int continuationByte = byteArray [byteIndex] & 0xFF;
        byteIndex++;
        
        if ((continuationByte & 0xC0) == 0x80) {
            return continuationByte & 0x3F;
        }
        
        throw new UTF8Exception ("Invalid continuation byte");
    }
    
    static string Ucs2Encode (int[] array)
    {
        StringBuilder output = new StringBuilder ();
        for (int i = 0; i < array.Length; i++) {
            int value = array[i];
            if (value > 0xFFFF) {
                value -= 0x10000;
                output.Append ((char)(value >> 10 & 0x3FF | 0xD800));
                value = 0xDC00 | value & 0x3FF;
            }
            output.Append ((char) value);
        }
        return output.ToString ();
    }
    
    private UTF8 ()
    {
    }
}
