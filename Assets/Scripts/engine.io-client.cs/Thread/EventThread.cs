﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace EngineIO
{
    // TODO more rubst implementaion...
    
    public class EventThread
    {
        public delegate void EventThreadDelegate ();
        
        private static Thread thread = null;
        private static BlockingQueue<EventThreadDelegate> _queue =
            new BlockingQueue<EventThreadDelegate>();
        private static object sync = new object ();
        
        private EventThread ()
        {
        }
        
        public static bool IsCurrent {
            get {
                return thread != null &&
                    Thread.CurrentThread.ManagedThreadId == thread.GetHashCode ();
            }
        }
        
        public static void Exec (EventThreadDelegate task)
        {
            if (IsCurrent) {
                task ();
            } else {
                NextTick (task);
            }
        }
        
        public static void NextTick (EventThreadDelegate task)
        {
            if (thread == null) {
                lock (sync) {
                    thread = new Thread (() => {
                        while (true) {
                            if (!_queue.IsEmpty) {
                                EventThreadDelegate _task = _queue.Dequeue ();
                                try {
                                    _task ();
                                } catch (Exception e) {}
                            }
                        }
                    });
                }
                thread.Start ();
            }
            
            _queue.Enqueue (task);
        }
    }
    
    public class BlockingQueue<T>
    {
        private Queue<T> _queue = new Queue<T> ();
        private Semaphore _gate = new Semaphore (0, Int32.MaxValue);
        
        public void Enqueue (T item)
        {
            lock (_queue) {
                _queue.Enqueue (item);
            }
            _gate.Release ();
        }
        
        public T Dequeue ()
        {
            _gate.WaitOne ();
            lock (_queue) {
                return _queue.Dequeue ();
            }
        }
        
        public bool IsEmpty {
            get {
                return _queue.Count == 0;
            }
        }
    }
}