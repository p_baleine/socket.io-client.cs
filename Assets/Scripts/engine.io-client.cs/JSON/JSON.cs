﻿using System.Collections;

namespace EngineIO
{
    public class JSON
    {
        public static object Parse (string json)
        {
            return MiniJSON.Json.Deserialize (json);
        }

        public static string Stringify (object obj)
        {
            return MiniJSON.Json.Serialize (obj);
        }
    }
}